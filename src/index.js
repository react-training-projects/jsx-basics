// import React and ReactDOM libraries
import React from 'react';
import ReactDOM from 'react-dom';

function getButtonText() {
  return 'Click me!';
}

// create a React component
const App = () => {
  const buttonStyle = {
    backgroundColor: 'blue',
    color: 'white'
  };

  return (
    <div>
      <label htmlFor="name" className="label">Enter name:</label>
      <input id="name" type="text" />
      <button style={buttonStyle}>{getButtonText()}</button>
    </div>
  );
}

// show component on screen
ReactDOM.render(
  <App />,
  document.querySelector('#root')
);